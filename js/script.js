function fetchFilms() {
  return fetch("https://ajax.test-danit.com/api/swapi/films").then((response) =>
    response.json()
  );
}

function fetchCharacter(url) {
  return fetch(url)
    .then((response) => response.json())
    .then((character) => character.name);
}

function displayFilms() {
  fetchFilms().then((films) => {
    const filmsContainer = document.getElementById("films");

    films.forEach((film) => {
      const filmElement = document.createElement("div");
      filmElement.classList.add("film");

      const filmHeader = document.createElement("h2");
      filmHeader.textContent = `Episode ${film.episodeId}: ${film.name}`;
      filmElement.appendChild(filmHeader);

      const filmDescription = document.createElement("p");
      filmDescription.textContent = film.openingCrawl;
      filmElement.appendChild(filmDescription);

      const charactersContainer = document.createElement("div");
      charactersContainer.classList.add("characters");
      charactersContainer.textContent = "Characters:";
      filmElement.appendChild(charactersContainer);

      film.characters.forEach((characterUrl) => {
        fetchCharacter(characterUrl).then((characterName) => {
          const characterElement = document.createElement("p");
          characterElement.textContent = characterName;
          charactersContainer.appendChild(characterElement);
        });
      });

      filmsContainer.appendChild(filmElement);
    });
  });
}

displayFilms();
